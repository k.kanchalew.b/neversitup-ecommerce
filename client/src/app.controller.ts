import {
  Controller,
  Get,
  Inject,
  Param,
  Post,
  Body,
  Delete,
} from '@nestjs/common';
import { AppService } from './app.service';
import { ClientProxy } from '@nestjs/microservices';

@Controller()
export class AppController {
  constructor(
    @Inject('ORDER_SERVICE')
    private orderService: ClientProxy,
    @Inject('PRODUCT_SERVICE')
    private productService: ClientProxy,
    @Inject('USER_SERVICE')
    private userService: ClientProxy,
  ) { }

  @Post('orders')
  async createOrder(@Body() { data }): Promise<any> {
    return this.orderService.send('createOrder', data);
  }

  @Delete('orders/:id')
  async deleteOrder(@Param() id: string): Promise<any> {
    return this.orderService.send('deleteOrderById', id);
  }

  @Get('orders')
  async getOrderDetailById(@Param() id: string): Promise<any> {
    return this.orderService.send('getOrderDetailById', id);
  }

  @Get('products')
  async getProducts(): Promise<any> {
    return this.productService.send('getProducts', null);
  }

  @Get('products/:id')
  async getProductDetailById(@Param('id') id: string): Promise<any> {
    return this.productService.send('getProductDetailById', id);
  }

  @Get('users/:id/profile')
  async getProfile(@Param('id') id: string): Promise<any> {
    return this.userService.send('getProfileById', id);
  }

  @Get('users/:id/orders')
  async getOrdersByUserId(@Param('id') id: string): Promise<any> {
    return this.userService.send('getOrdersByUserId', id);
  }

  @Post('login')
  async login(@Body() { data }): Promise<any> {
    return this.userService.send('login', data);
  }

  @Post('register')
  async register(@Body() { data }): Promise<any> {
    return this.userService.send('register', data);
  }
}
