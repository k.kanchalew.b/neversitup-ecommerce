import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { MessagePattern } from '@nestjs/microservices';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) { }

  @MessagePattern('login')
  async login(): Promise<any> {
    return new Promise(
      (v) => { }
    );
  }

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
}
